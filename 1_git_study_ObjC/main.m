//
//  main.m
//  1_git_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/11/29.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
