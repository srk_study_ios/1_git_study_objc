//
//  AppDelegate.h
//  1_git_study_ObjC
//
//  Created by 鶴本 幸大 on 2016/11/29.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

